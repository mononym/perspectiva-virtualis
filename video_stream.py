"""
Perspectiva Virtualis
Copyright (C) 2020 Julien Rippinger

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from utils.image_tools import PerspectivaVirtualis


def main():
    PV = PerspectivaVirtualis(timer=10)
    print(PV)
    while PV.camera_stream.isOpened():
        # // ____________________________________________________ DETECTION
        if PV.detection():
            # // ___________________________________________________ ADD AR
            PV.draw_sections()
        # // ________________________________________________ DISPLAY VIDEO
        PV.show()


if __name__ == "__main__":
    print("\n\t\t-[ START ]- \n")
    main()
    print("\n\t\t-[ GOODBYE ]-")
