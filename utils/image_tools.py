"""
Perspectiva Virtualis
Copyright (C) 2020 Julien Rippinger

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from cv2 import waitKey, resize, cvtColor, GaussianBlur, VideoCapture,\
    destroyAllWindows, threshold, morphologyEx, findContours, boundingRect, \
    circle, rectangle, bitwise_not, addWeighted, imshow, putText, \
    bitwise_and, namedWindow, setWindowProperty, moveWindow, flip, bitwise_or,\
    CAP_PROP_FRAME_WIDTH, CAP_PROP_FRAME_HEIGHT, \
    CAP_PROP_FPS, FONT_HERSHEY_COMPLEX, COLOR_RGB2RGBA, COLOR_BGR2GRAY, \
    WND_PROP_FULLSCREEN, WINDOW_FULLSCREEN, LINE_AA, THRESH_BINARY_INV, \
    THRESH_OTSU, MORPH_CLOSE, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE
from numpy import array, copy, ones, uint8, zeros
from operator import itemgetter
from imutils import grab_contours
from skimage.metrics import structural_similarity
from datetime import datetime, timedelta
from utils.model_tools import ModelDataHandler


class PerspectivaVirtualis():
    def __init__(self,
                 timer=20,
                 image_size=(1280, 720),
                 reduction_factor=4,
                 blur_kernel=(15, 15),
                 blur_sigma=0,
                 closing_kernel=(10, 10)):
        # video in
        self.camera_stream = self._start_camera(image_size)
        # image reduction factor
        self.reduction_factor = reduction_factor  # 6 for 1080
        self.blur_kernel = blur_kernel
        self.blur_sigma = blur_sigma
        self.closing_kernel = closing_kernel
        # detection image size
        # x value
        self.detection_image_width = (self.stream_width
                                      // self.reduction_factor)
        # y value
        self.detection_image_height = (self.stream_height
                                       // self.reduction_factor)
        # import models
        self.Models = ModelDataHandler(image_size)
        # prepare for detection
        self._capture_background()
        # model-loop timer
        self.timer = timer
        self.next_object_t = datetime.now() + timedelta(seconds=self.timer)
        # fullscreen (optional)
        namedWindow("Video Stream", WND_PROP_FULLSCREEN)
        # if using a second screen, move output video
        # by dimension of primary (left) display
        moveWindow("Video Stream", 0, 0)
        setWindowProperty(
            "Video Stream", WND_PROP_FULLSCREEN, WINDOW_FULLSCREEN)

    # // _______________ Main AR functions __________________________________

    def detection(self):
        # detect image changes
        self._capture_frame()
        self.thresh, all_rectangles = self._spot_pixel_differences()

        # spot and localise people
        self.people_rectangles,\
            self.z_buffer_values,\
            self.del_rectangles = self._locate_people(all_rectangles)

        return self.z_buffer_values != []

    def draw_sections(self):

        ghost_base = self.Models.get_ghost()

        # symmetry between section list and rectangles list
        # necessary for mask substraction
        section_indexes = sorted([gray//4 for gray in self.z_buffer_values],
                                 reverse=False)
        self.people_rectangles = sorted(
            self.people_rectangles, key=itemgetter(1))

        # create section mask
        section_mask = bitwise_not(self.thresh)  # todo precision
        section_mask = resize(
            section_mask, (self.stream_width, self.stream_height))

        # delete deselected rectangles from mask
        # *WARNING* persons in front of mask are not substracted from sections
        # in installation, ever user has to be visible head to foot
        for (x, y, w, h) in self.del_rectangles:
            x -= 3
            y += 3
            h += 6
            w += 6
            section_mask[y - h:y, x:x + w] = 255

        # substract detection from ghost
        zero = zeros(section_mask.shape, dtype=uint8)
        ghost_mask = resize(bitwise_or(section_mask, zero),
                            (self.stream_width, self.stream_height))
        ghost = bitwise_and(ghost_base, ghost_base, mask=ghost_mask)

        # adding sections to live frame with mask substraction
        for i, (x, y, w, h) in zip(section_indexes, self.people_rectangles):
            # get section
            section = self.Models.get_section(i)
            # alt: section = sections[i]
            # mask section then add to video stream
            section = bitwise_and(section, section, mask=section_mask)
            frame = addWeighted(self.live_frame, 1, section, 0.5, 1)
            # delete person from mask
            section_mask[y-h:y, x:x+w] = 255

        # add ghost
        frame = addWeighted(frame, 1, ghost, .1, 0)

        self.live_frame = frame

    def show(self):
        """display video frame and add interface controls"""
        imshow("Video Stream", self._get_live_frame())
        self._controls()
        if self._check_timer():  # automatic change to next model
            print("[MODEL]\tautoselect next model")
            self.Models.next()

    # // _______________ AR helper functions _________________________________

    def _spot_pixel_differences(self):
        # split live stream
        detection_frame = copy(self.live_frame)
        # transform images
        detection_frame = resize(detection_frame,
                                 (self.detection_image_width,
                                  self.detection_image_height))
        detection_frame = cvtColor(detection_frame, COLOR_BGR2GRAY)
        detection_frame = GaussianBlur(
            detection_frame, self.blur_kernel, self.blur_sigma)

        # compute the Structural Similarity Index (SSIM) between the two
        # images, ensuring that the difference image is returned
        # OPTION: _, diff = structural_similarity(detection_frame, background,
        # full=True, gaussian_weights=True)
        _, diff = structural_similarity(
            detection_frame, self.background, full=True)
        difference_image = (diff * 255).astype(uint8)  # was int32

        # threshold the difference image
        thresh_mask = threshold(difference_image, 0, 255,
                                THRESH_BINARY_INV | THRESH_OTSU)[1]
        # morphing
        kernel = ones(self.closing_kernel, uint8)
        closing = morphologyEx(thresh_mask, MORPH_CLOSE, kernel)

        # finding contours
        # before closing was copied…
        contours = findContours(closing, RETR_EXTERNAL,
                                CHAIN_APPROX_SIMPLE)

        # obtain the regions of the two input images that differ
        contours = grab_contours(contours)
        rectangles = array([boundingRect(c) for c in contours], uint8)

        return closing, rectangles

    def _locate_people(self, rectangles, show=True):
        z_buffer_mask = self.Models.get_zbuffer()

        people = []
        people_append = people.append
        del_rectangles = []  # is detected but not people
        del_rectangles_append = del_rectangles.append
        z_buffer_values = []
        z_buffer_values_append = z_buffer_values.append

        # loop over the contours and eliminate noise
        for (x, y, w, h) in rectangles:

            # todo fine tuning, find feed: h -= h // 10

            # resize to live frame
            x *= self.reduction_factor
            y *= self.reduction_factor
            h *= self.reduction_factor
            w *= self.reduction_factor
            y += h  # bottom line of rectangle

            if show:
                # draw all detection rectangles
                rectangle(self.live_frame, (x, y),
                          (x + w, y - h), (0, 0, 255), 1)

            # *WARNING* pixel != index
            # rectangle (x,y + h)     -> down left  -> index[y+ h-1][x-1]
            # rectangle (x + w,y + h) -> down right -> index[y+ h-1][x+w-1]

            # if apha not zero, person is inside the object zone
            if z_buffer_mask[y - 1, x - 1][3] == 0 \
                    and z_buffer_mask[y - 1, x + w - 1][3] == 0:
                del_rectangles_append((x, y, w, h))
                continue

            # rectangle area
            # todo fine tuning, detect kids?
            elif w * h < 20000:
                del_rectangles_append((x, y, w, h))
                continue

            else:
                # get zbuffer gray values
                gray = z_buffer_mask[y - 1][self.stream_width // 2][0]
                # central projection
                z_buffer_values_append(gray)
                # keep people rectangles for section masking
                people_append((x, y, w, h))

                if show:
                    # draw zbuffer coordinates
                    circle(self.live_frame,
                           (self.stream_width // 2, y), 5, (255, 0, 255), -1)

                    # draw detection rectangles
                    rectangle(self.live_frame, (x, y),
                              (x + w, y - h), (0, 255, 0), 2)

                    # show detection size
                    text = str(w * h)
                    putText(self.live_frame, text, (x, y),
                            FONT_HERSHEY_COMPLEX, 1, (0, 255, 0), 1, LINE_AA)

        return people, z_buffer_values, del_rectangles

    # // _____________________ Helper functions ______________________________

    def _start_camera(self, image_size):
        stream = VideoCapture(0)  # webcam = 0
        # check if camera is connected
        assert (stream.read()[0])
        # same as model data
        stream.set(CAP_PROP_FRAME_WIDTH, image_size[0])
        stream.set(CAP_PROP_FRAME_HEIGHT, image_size[1])
        # image properties
        self.stream_width = int(
            stream.get(CAP_PROP_FRAME_WIDTH))
        self.stream_height = int(
            stream.get(CAP_PROP_FRAME_HEIGHT))
        self.stream_fps = stream.get(CAP_PROP_FPS)

        return stream

    def _capture_background(self):
        frame = self.camera_stream.read()[1]
        # prepare frame for detection
        frame = cvtColor(frame, COLOR_BGR2GRAY)
        frame = resize(frame,
                       (self.detection_image_width,
                        self.detection_image_height))
        frame = GaussianBlur(frame, self.blur_kernel, self.blur_sigma)
        self.background = frame

    def _capture_frame(self):
        frame = self.camera_stream.read()[1]
        # Alpha channel necessary for sections
        self.live_frame = cvtColor(frame, COLOR_RGB2RGBA)

    def _get_live_frame(self):
        # mirroring video
        frame = flip(self.live_frame, 1)
        # write info on screen
        putText(frame, self.Models.current, (20, self.stream_height-25),
                FONT_HERSHEY_COMPLEX, 3, (255, 255, 255), 1, LINE_AA)
        return frame

    def _controls(self):
        """reset background, change object or terminate stream"""
        key = waitKey(1)
        if key == 32:  # space bar
            self._capture_background()  # new background
            print("[DETN]\tbackground frame captured")

        if key == 9:  # horizontal tab
            print("[MODEL]\tmanually select next model")
            self.Models.next()  # use next model

        if key == 27:  # ESC
            print("[INFO]\tending stream")
            self.camera_stream.release()  # closes the stream
            destroyAllWindows()

    def _check_timer(self):
        """check current model display duration"""
        trigger = False
        if datetime.now() > self.next_object_t:
            trigger = True
            self.next_object_t = datetime.now() + timedelta(seconds=self.timer)
        return trigger

    def __str__(self):
        s = ("[VIDEO] fps: {: .2f}\n"
             "[VIDEO] stream_width: {}\n"
             "[VIDEO] stream_height: {}\n"
             "[DETN] reduction_factor: {}\n"
             "[DETN] detection_image_width: {}\n"
             "[DETN] detection_image_height: {}\n"
             "[DETN] blur_kernel: {}\n"
             "[DETN] blur_sigma: {}\n"
             "[DETN] closing_kernel: {}\n"
             "[MODEL] 3D models loaded: {}"
             ).format(self.stream_fps,
                      self.stream_width,
                      self.stream_height,
                      self.reduction_factor,
                      self.detection_image_width,
                      self.detection_image_height,
                      self.blur_kernel,
                      self.blur_sigma,
                      self.closing_kernel,
                      self.Models.keys)

        return s
