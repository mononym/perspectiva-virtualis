"""
Perspectiva Virtualis
Copyright (C) 2020 Julien Rippinger

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import os
from glob import glob
from cv2 import imread, resize, IMREAD_UNCHANGED


class ModelDataHandler():
    """
    Handle model ModelData
    """

    def __init__(self, image_size):
        self.path = os.getcwd()
        # sys.path[0]
        self.image_size = image_size
        self.sections_collection = self._load_sections()
        self.zbuffers = self._load_zbuffers()
        self.ghosts = self._load_ghosts()
        self.keys = list(self.sections_collection.keys())
        self.current = self.keys[0]
        self._update_model_data()

    def next(self):
        i = self.keys.index(self.current)
        i = i+1 if (i+2) < len(self.keys) else 0
        self.current = self.keys[i]
        self._update_model_data()

    def get_ghost(self):
        return self.ghost

    def get_zbuffer(self):
        return self.zbuffer

    def get_section(self, i):
        return self.sections[i]

    def _update_model_data(self):
        self.sections = self.sections_collection[self.current]
        self.ghost = self.ghosts[self.current]
        self.zbuffer = self.zbuffers[self.current]

    def _check_image_size(self, image):
        return image if ((len(image), len(image[0])) == self.image_size)\
            else resize(image, self.image_size)

    def _load_sections(self):
        """get sections from data directory"""

        object_sections = {}
        for object in glob(self.path + '/model_data/sections/*'):
            sections = []
            for section in sorted(glob(object+'/*.png')):
                image = self._check_image_size(imread(section,
                                                      IMREAD_UNCHANGED))
                sections.append(image)

            object_sections[os.path.basename(object)] = sections

        return object_sections

    def _load_zbuffers(self):
        """get zbuffer from data directory"""

        zbuffer = {}
        for object in glob(self.path + '/model_data/zbuffer/*.png'):
            image = self._check_image_size(imread(object,
                                                  IMREAD_UNCHANGED))

            # omit file extension .png
            zbuffer[os.path.basename(object)[:-4]] = image

        return zbuffer

    def _load_ghosts(self):
        """get ghosts from data directory"""

        ghosts = {}
        for object in glob(self.path+'/model_data/ghost/*.png'):
            image = self._check_image_size(imread(object,
                                                  IMREAD_UNCHANGED))
            # omit file extension .png
            ghosts[os.path.basename(object)[:-4]] = image

        return ghosts
