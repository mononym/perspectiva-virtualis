"""
Perspectiva Virtualis
Copyright (C) 2020 Julien Rippinger

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

'''###############################
# Section Script

Needs the addon cross_section.py
Needs Zbuffer plane as Plane

Made on blender 2.79

################################'''

import bpy
D = bpy.data
O = bpy.ops
C = bpy.context

# INPUTS
Object = 'Cube'
Cutter = 'Cutter'
Nbr_sections = 64


# 1• Select objects
O.object.select_all(action='DESELECT')
obj = D.objects[Object]
cut = D.objects[Cutter]

obj.select = True
cut.select = True

C.scene.objects.active = cut

# 2• Move Plane and section
Distance = D.objects['Plane'].dimensions.x
dist = Distance/Nbr_sections
x = cut.location.x
sections = []
Z = 20

for i in range(Nbr_sections):

    cut.location.x = x+i*dist

    bpy.ops.object.cross_section()

    sections.append(C.scene.objects[0])


for i in range(Nbr_sections):

    z = sections[i].location.z

    sections[i].location.z = z+Z
    sections[i].keyframe_insert(data_path='LOCATION',
                                index=-1,
                                frame=i)

    sections[i].location.z = z
    sections[i].keyframe_insert(data_path='LOCATION',
                                index=-1,
                                frame=i+1)

    section[i].location.z = z+Z
    sections[i].keyframe_insert(data_path='LOCATION',
                                index=-1,
                                frame=i+2)
