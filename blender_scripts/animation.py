"""
Perspectiva Virtualis
Copyright (C) 2020 Julien Rippinger

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import bpy
D = bpy.data

layerIndex = 0  # Chosen layer
objects = bpy.context.scene.objects

selectedObjects1 = [
    ob for ob in objects if ob.layers[layerIndex] and ob.select]

layerIndex = 1
selectedObjects2 = [
    ob for ob in objects if ob.layers[layerIndex] and ob.select]

print(len(selectedObjects1), len(selectedObjects2))

Object = list(selectedObjects1+selectedObjects2)

print(len(Object))


def Tri(X):
    return X.location.x


Objects = sorted(Object, key=Tri)


for i in range(len(Objects)):

    _i = i
    i = i+1

    bpy.context.scene.frame_current = i-1
    Objects[_i].hide_render = True
    Objects[_i].hide = True
    Objects[_i].keyframe_insert(data_path='hide_render',
                                index=-1,
                                frame=i-1)
    Objects[_i].keyframe_insert(data_path='hide',
                                index=-1,
                                frame=i-1)

    bpy.context.scene.frame_current = i
    Objects[_i].hide_render = False
    Objects[_i].hide = False
    Objects[_i].keyframe_insert(data_path='hide_render',
                                index=-1,
                                frame=i)
    Objects[_i].keyframe_insert(data_path='hide',
                                index=-1,
                                frame=i)

    bpy.context.scene.frame_current = i+1
    Objects[_i].hide_render = True
    Objects[_i].hide = True
    Objects[_i].keyframe_insert(data_path='hide_render',
                                index=-1,
                                frame=i+1)
    Objects[_i].keyframe_insert(data_path='hide',
                                index=-1,
                                frame=i+1)
