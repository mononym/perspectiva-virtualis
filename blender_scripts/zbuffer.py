"""
Perspectiva Virtualis
Copyright (C) 2020 Julien Rippinger

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

'''###############################
# Zbuffer Script

Needs to set the Origin of Plane at its first edge
IN EDIT MODE >>> snap cursor to selected
IN OBJECT MODE >>> set origin >>> origin to 3D Cursor

Made on blender 2.79

################################'''

import bpy
D = bpy.data
O = bpy.ops
C = bpy.context

# INPUTS
Surface = 'Plane'
Nbr_sections = 64

# 1• Rescale Surface in y
O.object.select_all(action='DESELECT')
surface = D.objects[Surface]
surface.select = True
bpy.context.scene.objects.active = surface

x = surface.dimensions.x
X = x/Nbr_sections
surface.dimensions.x = X

# 2• Copy Surface along x with zbuffer material
for i in range(Nbr_sections):
    O.object.select_all(action='DESELECT')
    surface.select = True
    bpy.context.scene.objects.active = surface

    O.object.duplicate()
    copy = C.scene.objects[0]
    copy.location.x += i*X

    color = [(1/(Nbr_sections-1))*i]*3
    mat = D.materials.new('mat'+str(i))

    mat.diffuse_color = color
    mat.use_shadeless = True

    copy.active_material = mat

surface.dimensions.x = X
