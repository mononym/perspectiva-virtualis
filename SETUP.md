## Setup

### Installation Space

![setup](./source/images/setup.png)

Besides a computer, the installation consists of a webcam, a projector and a projection screen. The webcam faces the space where the user will interact with the installation. The projector casts the webcams stream in the opposite direction. The projection screen thus works as a kind of digital mirror.

### Model Calibration

Once the projection set up, capture an image of the space from the webcam and calibrate the camera position with [fSpy](https://fspy.io/).

![calibration](./source/images/calibration.png)

Parameters to be selected:
+ 1 vanishing point
+ (-x, -z) axes
+ reference distance in **metres**

### Image Extraction

Install the [fSpy addon](https://github.com/stuffmatic/fSpy-Blender/releases/tag/v1.0.2) in **Blender version 2.79**. Import the calibrated camera into a Blender file containing a 3D model: `./model/Nakagin_Capsule.blend`.

To obtain the necessary images from the 3D model, follow the steps from the text file in the Nakagin_Capsule.blend file.


#### Example Images to be Obtained
zbuffer:
![zbuffer](./source/images/zbuffer_copy.png)
section (here n°28 and without transparency):
![section](./source/images/section_copy.png)
ghost (here without transparency):
![ghost](./source/images/ghost_copy.png)

### File Structure

Save the rendered images in the following file structure:

```
└─── model_data
    └─── ghost
    │   │   Nakagin Capsule.png
    │   │   ...
    │
    └─── sections
    │       └─── Nakagin Capsule
    │       │   │   0001.png
    │       │   │   0002.png
    │       │   │   ...
    │       │
    │       └─── ...
    │
    └─── zbuffer
        │   Nakagin Capsule.png
        │   ...
```
Each model has its own section, ghost and zbuffer image.
