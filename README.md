[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.5040580.svg)](https://doi.org/10.5281/zenodo.5040580)
[![Blender](https://img.shields.io/badge/Blender-2.79-orange)](https://download.blender.org/release/Blender2.79/)
[![License](https://img.shields.io/badge/License-AGPL--3.0-informational)](https://www.gnu.org/licenses/agpl-3.0.html)

# Perspectiva Virtualis

## :warning: DEPRECATED -> Moved to https://codeberg.org/mononym/perspectiva-virtualis
