[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.5040580.svg)](https://doi.org/10.5281/zenodo.5040580)
[![Blender](https://img.shields.io/badge/Blender-2.79-orange)](https://download.blender.org/release/Blender2.79/)
[![License](https://img.shields.io/badge/License-AGPL--3.0-informational)](https://www.gnu.org/licenses/agpl-3.0.html)

# Perspectiva Virtualis

![installation](./source/images/artificial-realities_lisbon.JPG)

Perspectiva Virtualis proposes a distinct type of augmented reality in which perspective projection acts as a hinge between its constituent elements. The installation articulates the different projective features of the observer’s perception, of the camera/screen pairing and of the augmenting imagery. The device itself acts as a digital mirror – the screen becomes a reversed window opening towards the observer. Depending on his linear movement towards the screen, the user can witness his reflection slicing a virtual architectural object. These sections – i.e. the augmenting imagery – are in perspective and calibrated to the camera’s one-eyed central perspective.

**Featured in:**
+ **Master Dissertation**: Lachard, Arthur. *Perspectiva Virtualis : asymétrie, mimêsis, épiphanie*. Université libre de Bruxelles, 2016.
+ **Winning Project**: gURROO.com competition – Innovative Minds 2016: Virtual Epoch.
+ **Exhibition**: *Artificial Realities // Lisbon Architecture Triennale Associated Project*. ISTAR – Information Sciences and Technologies and Architecture Research Centre, ISCTE – University Institute of Lisbon, 14–18 October 2019.
+ [**Book**](https://www.routledge.com/Virtual-Aesthetics-in-Architecture-Designing-in-Mixed-Realities/Eloy-Kreutzberg-Symeonidou/p/book/9781032023731#): Rippinger, Julien, and Arthur Lachard. “Perspectiva Virtualis.” In _Virtual Aesthetics in Architecture: Designing in Mixed Realities_, edited by Sara Eloy, Anette Kreutzberg, and Ioanna Symenidou. Routledge, 2021.

![virtual aesthetics](https://images.routledge.com/common/jackets/crclarge/978103202/9781032023731.jpg)

## Citation
Rippinger, Julien, & Lachard, Arthur. (2020). Perspectiva Virtualis. Zenodo. https://doi.org/10.5281/zenodo.5040580

```
@software{Rippinger2020,
  title = {Perspectiva {{Virtualis}}},
  author = {Rippinger, Julien and Lachard, Arthur},
  date = {2020-12-07},
  doi = {10.5281/ZENODO.5040580},
  url = {https://zenodo.org/record/5040580},
  abstract = {AR installation to explore the combination of perspective projection and architectural representation.},
  isbn = {978-1-03-202373-1},
  organization = {{Zenodo}}
}
```

## Quickstart

### Video

Position your webcam in front of an empty space at around 170 cm high and slightly rotated towards the ground (5°).

### Requirements

#### (recommended) Make and Activate Virtual Environment
```
python -m venv env
cd env
source bin/activate
```
#### Install Requirements
```
pip install opencv-contrib-python scikit-image imutils
```
### Start Installation
```
python video_stream.py
```

#### Controls

+ Space bar: capture new background
+ Horizontal tab: switch to next model
+ Esc: terminate

### Custom setup

Ideally, the webcam should be placed at the eye’s height. But because of the projectors light beams, the camera is placed under or above the projection screen (as seen in the above photograph). Therefore, it is necessary to set up the installation starting with [calibrating](SETUP.md) the cameras’ position.

## Under the Hood

![process](./source/images/process.png)

1. Automatic capture of a background image (of an empty installation)
2. Detection of pixel differences between the background image and the video live feed.
3. Locate the detected persons on the zbuffer image. The zbuffer value corresponds to the grey value from the zbuffer png image.
4. Select and add the corresponding section (by zbuffer value) from the sections list.

beta version in Pure Data:
![beta](./source/images/beta.png)
[video](https://vimeo.com/183805093)

## TODO
- [ ] Automatic camera calibration with markers.
- [ ] Real-time rendering of sections.
- [ ] Port Blender Python scripts to current version.
